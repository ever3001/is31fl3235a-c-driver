#ifndef _IS31FL3235A_SETUP_H_
#define _IS31FL3235A_SETUP_H_

// Please see the datasheet for more info.
// The possible values for the AD are: IS31FL3235A_SLAVE_AD_{VCC,GND,SDA,SCL}
#define IS31FL3235A_AD (IS31FL3235A_SLAVE_AD_VCC)

#endif // _IS31FL3235A_SETUP_H_