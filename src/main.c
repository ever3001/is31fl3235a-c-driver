#include <stdio.h>

#include "IS31FL3235A.h"
#include <stdbool.h>

bool test_reset_register()
{
	reset_registers();
	// TEST SHUTDOWN REGISTER
	if (get_shutdown_reg().data != IS31FL3235A_REG_DEFAULT_VAL)
	{
		return false;
	}
	// TEST PWM OUT AND LED CONTROL REGISTERS
	for (uint8_t i = 0; i < IS31FL3235A_OUT_MAX; ++i)
	{
		if (get_pwm_out_reg(i).data != IS31FL3235A_REG_DEFAULT_VAL)
		{
			return false;
		}
		if (get_led_control_reg(i).data != IS31FL3235A_REG_DEFAULT_VAL)
		{
			return false;
		}
	}
	// TEST GLOBAL CONTROL REGISTER
	if (get_global_control_reg().data != IS31FL3235A_REG_DEFAULT_VAL)
	{
		return false;
	}
	// TEST OUTPUT FREQUENCY SETTING REGISTER
	if (get_output_frequency_setting_reg().data != IS31FL3235A_REG_DEFAULT_VAL)
	{
		return false;
	}
	return true;
}

bool test_shutdown_mode_register()
{
	set_shutdown_mode(IS31FL3235A_SHUTDOWN_MODE_NORMAL_OPERATION);
	uint8_t mode = get_shutdown_reg().data;
	if (mode != IS31FL3235A_SHUTDOWN_MODE_NORMAL_OPERATION)
	{
		return false;
	}
	set_shutdown_mode(IS31FL3235A_SHUTDOWN_MODE_SOFT_SHUTDOWN_MODE);
	mode = get_shutdown_reg().data;
	if (mode != IS31FL3235A_SHUTDOWN_MODE_SOFT_SHUTDOWN_MODE)
	{
		return false;
	}
	set_shutdown_mode(IS31FL3235A_SHUTDOWN_MODE_NORMAL_OPERATION);
	return true;
}

bool test_pwm_out_registers()
{
	uint8_t data;
	for (IS31FL3235A_outputs_t i = 0; i < IS31FL3235A_OUT_MAX; ++i)
	{
		set_pwm_out(i, 0xFF);
		data = get_pwm_out_reg(i).data;
		if (data != 0xFF)
		{
			return false;
		}
		set_pwm_out(i, 0x00);
		data = get_pwm_out_reg(i).data;
		if (data != 0x00)
		{
			return false;
		}
		set_pwm_out(i, 0xFF);
	}
	return true;
}

bool test_led_control_registers()
{
	uint8_t data;
	for (IS31FL3235A_outputs_t i = 0; i < IS31FL3235A_OUT_MAX; ++i)
	{
		// TODO
	}
	return true;
}

bool test_global_control_register()
{
	uint8_t data;
	set_global_control(IS31FL3235A_GLOBAL_CTRL_REG_LED_ENABLE_SHUTDOWN_ALL_LEDS);
	data = get_global_control_reg().data;
	if (data != IS31FL3235A_GLOBAL_CTRL_REG_LED_ENABLE_SHUTDOWN_ALL_LEDS)
	{
		return false;
	}
	set_global_control(IS31FL3235A_GLOBAL_CTRL_REG_LED_ENABLE_NORMAL_OP);
	data = get_global_control_reg().data;
	if (data != IS31FL3235A_GLOBAL_CTRL_REG_LED_ENABLE_NORMAL_OP)
	{
		return false;
	}
	set_global_control(IS31FL3235A_GLOBAL_CTRL_REG_LED_ENABLE_SHUTDOWN_ALL_LEDS);
	return true;
}

bool test_output_frequency_setting_register()
{
	uint8_t data;
	set_output_frequency_setting(IS31FL3235A_OUT_FREQUENCY_SETTING_22KHZ);
	data = get_output_frequency_setting_reg().data;
	if (data != IS31FL3235A_OUT_FREQUENCY_SETTING_22KHZ)
	{
		return false;
	}
	set_output_frequency_setting(IS31FL3235A_OUT_FREQUENCY_SETTING_3KHZ);
	data = get_output_frequency_setting_reg().data;
	if (data != IS31FL3235A_OUT_FREQUENCY_SETTING_3KHZ)
	{
		return false;
	}
	set_output_frequency_setting(IS31FL3235A_OUT_FREQUENCY_SETTING_22KHZ);
	return true;
}

int main(void)
{
	printf("IS31FL3235A test\n");
	if(!test_reset_register())
	{
		printf("Reset register test failed\n");
		return 1;
	}
	if(!test_shutdown_mode_register())
	{
		printf("Shutdown mode register test failed\n");
		return 1;
	}
	if(!test_pwm_out_registers())
	{
		printf("PWM out register test failed\n");
		return 1;
	}
	if(!test_led_control_registers())
	{
		printf("LED control register test failed\n");
		return 1;
	}
	if(!test_global_control_register())
	{
		printf("Global control register test failed\n");
		return 1;
	}
	if(!test_output_frequency_setting_register())
	{
		printf("Output frequency setting register test failed\n");
		return 1;
	}
	if(!test_reset_register())
	{
		printf("Reset register test 2 failed\n");
		return 1;
	}
	printf("All tests passed\n");
	return 0;
}