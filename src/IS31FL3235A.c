#include "IS31FL3235A_Setup.h"
#include "IS31FL3235A.h"
#include "I2C_HAL.h"

IS31FL3235A_t _IS31FL3235A = {
    .slave_addr = ((0xF << 3) | (IS31FL3235A_AD << 2) | 0x0),
    .SHUTDOWN = {
        .addr = ADDR_SHUTDOWN,
        .data = IS31FL3235A_REG_DEFAULT_VAL},
    .PWM = {
        {.addr = ADDR_PWM_OUT01, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT02, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT03, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT04, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT05, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT06, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT07, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT08, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT09, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT10, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT11, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT12, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT13, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT14, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT15, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT16, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT17, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT18, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT19, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT20, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT21, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT22, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT23, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT24, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT25, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT26, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT27, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_PWM_OUT28, .data = IS31FL3235A_REG_DEFAULT_VAL},
    },
    .PWM_UPDATE = {.addr = ADDR_PWM_UPDATE, .data = IS31FL3235A_REG_DEFAULT_VAL},
    .LED_CONTROL = {
        {.addr = ADDR_LED_CONTROL_OUT01, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT02, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT03, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT04, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT05, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT06, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT07, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT08, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT09, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT10, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT11, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT12, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT13, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT14, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT15, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT16, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT17, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT18, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT19, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT20, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT21, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT22, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT23, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT24, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT25, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT26, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT27, .data = IS31FL3235A_REG_DEFAULT_VAL},
        {.addr = ADDR_LED_CONTROL_OUT28, .data = IS31FL3235A_REG_DEFAULT_VAL},
    },
    .GLOBAL_CONTROL = {.addr = ADDR_GLOBAL_CONTROL, .data = IS31FL3235A_REG_DEFAULT_VAL},
    .OUT_FREQ_SETTING = {.addr = ADDR_OUT_FREQ_SETTING, .data = IS31FL3235A_REG_DEFAULT_VAL},
    .RESET = {.addr = ADDR_RESET, .data = IS31FL3235A_REG_DEFAULT_VAL}};

IS31FL3235A_error_t write_register(const uint8_t reg_addr, uint8_t data)
{
    uint8_t tx_data[2] = {reg_addr, data};
    int error = HAL_I2C_Master_Transmit(_IS31FL3235A.slave_addr, tx_data, 2);
    if (error != I2C_HAL_OK)
    {
        return IS31FL3235A_FAIL_I2C_WRITE;
    }
    return IS31FL3235A_OK;
}

IS31FL3235A_error_t set_shutdown_mode(IS31FL3235A_shutdown_mode_t shutdown_mode)
{
    uint8_t data = shutdown_mode & 0x01;
    IS31FL3235A_error_t error = write_register(_IS31FL3235A.SHUTDOWN.addr, data);
    if (error != IS31FL3235A_OK)
    {
        error = IS31FL3235A_FAIL_SHUTDOWN_MODE;
    }
    else
    {
        _IS31FL3235A.SHUTDOWN.data = data;
    }
    return error;
}

IS31FL3235A_error_t set_pwm_out(IS31FL3235A_outputs_t out, uint8_t pwm_value)
{
    if(out > IS31FL3235A_OUT_28)
    {
        return IS31FL3235A_FAIL_OUTPUT_NUMBER;
    }
    IS31FL3235A_error_t error = write_register(_IS31FL3235A.PWM[out].addr, pwm_value);
    if (error != IS31FL3235A_OK)
    {
        error = IS31FL3235A_FAIL_PWM_UPDATE;
    }
    else
    {
        _IS31FL3235A.PWM[out].data = pwm_value;
    }
    return error;
}

IS31FL3235A_error_t pwm_update(void)
{
    IS31FL3235A_error_t error = write_register(_IS31FL3235A.PWM_UPDATE.addr, IS31FL3235A_REG_DEFAULT_VAL);
    if (error != IS31FL3235A_OK)
    {
        error = IS31FL3235A_FAIL_PWM_UPDATE;
    }
    return error;
}

IS31FL3235A_error_t set_and_update_pwm_out(IS31FL3235A_outputs_t out, uint8_t pwm_value)
{
    IS31FL3235A_error_t error = set_pwm_out(out, pwm_value);
    if (error == IS31FL3235A_OK)
    {
        error = pwm_update();
    }
    return error;
}

IS31FL3235A_error_t set_led_control(IS31FL3235A_outputs_t out, IS31FL3235A_led_ctrl_reg_iout_t iout, IS31FL3235A_led_ctrl_reg_led_state_t led_state)
{
    if(out > IS31FL3235A_OUT_28)
    {
        return IS31FL3235A_FAIL_OUTPUT_NUMBER;
    }
    uint8_t data = ((iout << 1) | led_state) & 0xFF;
    IS31FL3235A_error_t error = write_register(_IS31FL3235A.LED_CONTROL[out].addr, data);
    if (error != IS31FL3235A_OK)
    {
        error = IS31FL3235A_FAIL_LED_CTRL;
    }
    else
    {
        _IS31FL3235A.LED_CONTROL[out].data = data;
    }
    return error;
}

IS31FL3235A_error_t set_and_update_led_control(IS31FL3235A_outputs_t out, IS31FL3235A_led_ctrl_reg_iout_t iout, IS31FL3235A_led_ctrl_reg_led_state_t led_state)
{
    IS31FL3235A_error_t error = set_led_control(out, iout, led_state);
    if (error == IS31FL3235A_OK)
    {
        error = pwm_update();
    }
    return error;
}

IS31FL3235A_error_t set_global_control(IS31FL3235A_global_ctrl_reg_led_enable_t led_enable)
{
    uint8_t data = led_enable & 0x01;
    IS31FL3235A_error_t error = write_register(_IS31FL3235A.GLOBAL_CONTROL.addr, data);
    if (error != IS31FL3235A_OK)
    {
        error = IS31FL3235A_FAIL_GLOBAL_CTRL;
    }
    else
    {
        _IS31FL3235A.GLOBAL_CONTROL.data = data;
    }
    return error;
}

IS31FL3235A_error_t set_output_frequency_setting(IS31FL3235A_out_frequency_setting_t out_frequency_setting)
{
    uint8_t data = out_frequency_setting & 0x01;
    IS31FL3235A_error_t error = write_register(_IS31FL3235A.OUT_FREQ_SETTING.addr, data);
    if (error != IS31FL3235A_OK)
    {
        error = IS31FL3235A_FAIL_OUT_FREQ_SETTING;
    }
    else
    {
        _IS31FL3235A.OUT_FREQ_SETTING.data = data;
    }
    return error;
}

IS31FL3235A_error_t reset_registers(void)
{
    IS31FL3235A_error_t error = write_register(_IS31FL3235A.RESET.addr, IS31FL3235A_REG_DEFAULT_VAL);
    if (error == IS31FL3235A_OK)
    {
        _IS31FL3235A.SHUTDOWN.data = IS31FL3235A_REG_DEFAULT_VAL;
        _IS31FL3235A.PWM_UPDATE.data = IS31FL3235A_REG_DEFAULT_VAL;
        _IS31FL3235A.GLOBAL_CONTROL.data = IS31FL3235A_REG_DEFAULT_VAL;
        _IS31FL3235A.OUT_FREQ_SETTING.data = IS31FL3235A_REG_DEFAULT_VAL;
        for (uint8_t i = 0; i < IS31FL3235A_OUT_MAX; ++i)
        {
            _IS31FL3235A.PWM[i].data = IS31FL3235A_REG_DEFAULT_VAL;
            _IS31FL3235A.LED_CONTROL[i].data = IS31FL3235A_REG_DEFAULT_VAL;
        }
    }
    else
    {
        error = IS31FL3235A_FAIL_RESET;
    }
    return error;
}

const IS31FL3235A_reg_t get_shutdown_reg(void)
{
    return _IS31FL3235A.SHUTDOWN;
}

const IS31FL3235A_reg_t get_pwm_out_reg(IS31FL3235A_outputs_t out)
{
    return _IS31FL3235A.PWM[out];
}

const IS31FL3235A_reg_t get_led_control_reg(IS31FL3235A_outputs_t out)
{
    return _IS31FL3235A.LED_CONTROL[out];
}

const IS31FL3235A_reg_t get_global_control_reg(void)
{
    return _IS31FL3235A.GLOBAL_CONTROL;
}

const IS31FL3235A_reg_t get_output_frequency_setting_reg(void)
{
    return _IS31FL3235A.OUT_FREQ_SETTING;
}

const IS31FL3235A_t *get_IS31FL3235A(void)
{
    return &_IS31FL3235A;
}