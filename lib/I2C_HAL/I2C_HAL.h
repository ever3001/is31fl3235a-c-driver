#ifndef _I2C_HAL_H_
#define _I2C_HAL_H_

#include <stdint.h>

#define I2C_HAL_OK (0)
#define I2C_HAL_FAIL (-1)

int HAL_I2C_Master_Transmit(uint8_t addr, uint8_t *tx_data, uint16_t tx_size);

#endif // _I2C_HAL_H_