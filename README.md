[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

# IS31FL3235A C Driver

This is my C implementation of the IS31FL3235A Driver.

IS31FL3235A is comprised of 28 constant current channels each with independent PWM control, designed for driving LEDs.

## Contributing

If you would like to contribute, please see the [instructions](./CONTRIBUTING.md).

## License

This project is licensed under the MIT License - see the [LICENSE](./LICENSE) file for details.
